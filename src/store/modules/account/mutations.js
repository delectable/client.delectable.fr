import * as TYPES from './types'

export default {
  [TYPES.SET_AUTH_STATUS] (state, payload) {
    state.isAuthenticated = payload
  },
  [TYPES.SET_ACCOUNT_DATA] (state, payload) {
    state = { ...state, ...payload }
  }
}
