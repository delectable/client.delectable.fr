import * as TYPES from './types'

export default {
  [TYPES.SET_ACCOUNT_DATA] ({ commit }, payload) {
    commit(TYPES.SET_ACCOUNT_DATA, payload)
  },
  [TYPES.SET_AUTH_STATUS] ({ commit }, payload) {
    commit(TYPES.SET_AUTH_STATUS, payload)
  }
}
