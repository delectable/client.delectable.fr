import Vue from 'vue'
import Vuex from 'vuex'

import ui from './modules/ui'
import account from './modules/account'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    account,
    ui
  }
})
