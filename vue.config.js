const baseUrl = process.env.NODE_ENV === 'production' ? '/delectable/app.delectable.fr' : '/'
const path = require('path')

module.exports = {
  baseUrl: baseUrl,
  pluginOptions: {
    'style-resources-loader': {
      patterns: [
        './src/assets/styles/abstracts/variables.scss',
        './src/assets/styles/abstracts/**/*.scss'
      ],
      preProcessor: 'scss'
    }
  },

  configureWebpack: {
  },

  chainWebpack: config => {
    // EJS Loader
    config.module
      .rule('ejs')
      .test(/\.ejs$/)
      .use('ejs-loader')
      .loader('ejs-loader')
      .end()

    config
      .plugin('html')
      .tap(args => {
        args[0].template = 'public/index.ejs'
        args[0].inject = false
        args[0].hash = true
        args[0].minify = process.env.NODE_ENV === 'production'
        args[0].collapseWhitespace = true
        args[0].removeComments = true
        args[0].removeRedundantAttributes = true
        args[0].removeScriptTypeAttributes = true
        args[0].removeStyleLinkTypeAttributes = true
        args[0].useShortDoctype = true
        return args
      })
  }
}
