import * as TYPES from './types'

export default {
  [TYPES.SET_TOPBAR_COMPONENT] (state, payload) {
    state.topbar = payload
  }
}
