import actions from './actions'
import mutations from './mutations'
import * as TYPES from './types'

const state = {
  topbar: {
    component: null,
    data: null
  }
}

const getters = {
  [TYPES.GET_TOPBAR_COMPONENT]: state => state.topbar
}

export default {
  actions,
  getters,
  mutations,
  state,
  namespaced: true
}
