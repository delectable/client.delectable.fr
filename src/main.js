import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import { Component } from 'vue-property-decorator'

import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'

import App from '@/App.vue'

Vue.config.productionTip = false

// Register the custom hooks with their names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate'
])

// Keep vue-router and vuex synchronized
sync(store, router)

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
