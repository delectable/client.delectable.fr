import * as TYPES from './types'

export default {
  [TYPES.SET_TOPBAR_COMPONENT] ({ commit }, payload) {
    commit(TYPES.SET_TOPBAR_COMPONENT, payload)
  }
}
