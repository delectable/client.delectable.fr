import actions from './actions'
import mutations from './mutations'
import * as TYPES from './types'

const state = {
  id: null,
  firstname: null,
  lastname: null,
  login: null,
  email: null,
  isAuthenticated: false
}

const getters = {
  [TYPES.GET_AUTH_STATUS]: state => state.isAuthenticated,
  [TYPES.GET_ACCOUNT_DATA]: state => {
    return {
      id: state.id,
      firstname: state.firstname,
      lastname: state.lastname,
      login: state.login,
      email: state.email
    }
  }
}

export default {
  actions,
  getters,
  mutations,
  state,
  namespaced: true
}
