import router from './index'

import { DEFAULT_LAYOUT, MINIMAL_LAYOUT } from '@/layouts'
import store from '@/store'

import Feed from '@/views/feed/Feed'
import Explore from '@/views/explore/Explore'
import Search from '@/views/search/Search'
import Notifications from '@/views/notifications/Notifications'
import Profil from '@/views/profil/Profil'
import ProfilFeed from '@/views/profil/feed/Feed'
import ProfilFollowers from '@/views/profil/followers/Followers'
import ProfilFollowing from '@/views/profil/following/Following'
import SignIn from '@/views/sign-in/SignIn'
import SignUp from '@/views/sign-up/SignUp'
import PasswordForgot from '@/views/password-forgot/PasswordForgot'

import Settings from '@/views/settings/Settings'
import SettingsAccessibility from '@/views/settings/accessibility/Accessibility'
import SettingsBlockedUsers from '@/views/settings/blocked-users/BlockedUsers'
import SettingsFindFriends from '@/views/settings/find-friends/FindFriends'
import SettingsNotifications from '@/views/settings/notifications/Notifications'
import SettingsAccount from '@/views/settings/account/Account'

const authIsRequired = (to, from, next) => {
  const isAuthenticated = store.getters['account/GET_AUTH_STATUS']
  if (!isAuthenticated) {
    router.replace({ name: 'sign-in' })
  }
  next()
}

const routes = [
  {
    path: '/',
    name: 'feed',
    component: Feed,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 0
    }
  },
  {
    path: '/explore',
    name: 'explore',
    component: Explore,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 1
    }
  },
  {
    path: '/search',
    name: 'search',
    component: Search,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 2
    }
  },
  {
    path: '/notifications',
    name: 'notifications',
    component: Notifications,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 3
    }
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 5
    }
  },
  {
    path: '/settings/account',
    name: 'settings-account',
    component: SettingsAccount,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 5
    }
  },
  {
    path: '/settings/accessibility',
    name: 'settings-accessibility',
    component: SettingsAccessibility,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 5
    }
  },
  {
    path: '/settings/blocked-users',
    name: 'settings-blocked-users',
    component: SettingsBlockedUsers,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 5
    }
  },
  {
    path: '/settings/find-friends',
    name: 'settings-find-friends',
    component: SettingsFindFriends,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 5
    }
  },
  {
    path: '/settings/notifications',
    name: 'settings-notifications',
    component: SettingsNotifications,
    beforeEnter: authIsRequired,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 5
    }
  },
  {
    path: '/sign-in',
    name: 'sign-in',
    component: SignIn,
    meta: {
      layout: MINIMAL_LAYOUT,
      order: 5
    }
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: SignUp,
    meta: {
      layout: MINIMAL_LAYOUT,
      order: 5
    }
  },
  {
    path: '/password-forgot',
    name: 'password-forgot',
    component: PasswordForgot,
    meta: {
      layout: MINIMAL_LAYOUT,
      order: 5
    }
  },
  {
    path: '/:slug',
    component: Profil,
    meta: {
      layout: DEFAULT_LAYOUT,
      order: 4
    },
    children: [
      {
        path: '',
        name: 'profil-feed',
        component: ProfilFeed
      },
      {
        path: 'followers',
        name: 'profil-followers',
        component: ProfilFollowers
      },
      {
        path: 'following',
        name: 'profil-following',
        component: ProfilFollowing
      }
    ]
  }
]

export default routes
