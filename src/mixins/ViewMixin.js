import { Vue, Component } from 'vue-property-decorator'
import { Action } from 'vuex-class'

const config = {
  name: 'ViewMixin'
}

export default Component({ ...config })(
  class ViewMixin extends Vue {
    @Action('ui/SET_TOPBAR_COMPONENT') viewMixin_setTopbarComponent
  }
)
