module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],

  parserOptions: {
    parser: 'babel-eslint',
    ecmaFeatures: { legacyDecorators: true }
  },

  // required to lint *.vue files
  plugins: [
    'html'
  ],

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'arrow-parens': 0,
    'no-unreachable': 1,
    'no-unused-vars': 1,
    'no-multiple-empty-lines': 1,
    'generator-star-spacing': 0,
    'space-before-function-paren': 0,
    'indent': 'off',
    'camelcase': 'off',
    'vue/script-indent': ['warn', 2, {
      'baseIndent': 1
    }]
  }
}
