export const messages = {
  fr: {
    global: {
      views: {
        feed: {
          title: 'Accueil'
        },
        explore: {
          title: 'Découvrir',
          searchInputPlaceholder: 'Recherche Délectable'
        },
        notifications: {
          title: 'Notifications'
        },
        search: {
          title: 'Recherche',
          searchInputPlaceholder: 'Recherche Delectable'
        },
        profil: {
          title: 'Mon profil'
        },
        profilFollowers: {
          title: 'Personnes qui suivent'
        },
        profilFollowing: {
          title: 'Personnes suivies par'
        },
        settings: {
          title: 'Paramètres'
        },
        settingsAccount: {
          title: 'Compte'
        },
        settingsAccessibility: {
          title: 'Accessibilité'
        },
        settingsBlockedUsers: {
          title: 'Utilisateurs bloqués'
        },
        settingsFindFriends: {
          title: 'Retrouver des amis'
        },
        settingsNotifications: {
          title: 'Notifications'
        }
      },
      components: {
        addRecipe: {
          title: 'Ajouter une recette'
        }
      }
    }
  },
  en: {
    global: {
      views: {
        feed: {
          title: 'Home'
        },
        explore: {
          title: 'Explore'
        },
        notifications: {
          title: 'Notifications'
        },
        search: {
          title: 'Search',
          searchInputPlaceholder: 'Search Delectable'
        },
        profil: {
          title: 'My profil'
        },
        profilFollowers: {
          title: 'People following'
        },
        profilFollowing: {
          title: 'People followed by'
        },
        settings: {
          title: 'Settings'
        },
        settingsAccount: {
          title: 'Account'
        },
        settingsAccessibility: {
          title: 'Accessibility'
        },
        settingsBlockedUsers: {
          title: 'Blocked users'
        },
        settingsFindFriends: {
          title: 'Find friends'
        },
        settingsNotifications: {
          title: 'Notifications'
        }
      },
      components: {
        addRecipe: {
          title: 'Add recipe'
        }
      }
    }
  }
}
