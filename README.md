# Délectable Client
## Installation

### Pré-requis :
- NVM (avec Node 8.10.0)
- Yarn
- Git et Git Flow
- Cmder (pour les utilisateurs de Windows)

### Mise en place
```bash
# Pour les utilisateurs de Windows : utiliser Cmder

# cloner et atteindre le projet
git clone https://gitlab.com/delectable/client.delectable.fr.git
cd client.delectable.fr
# initialiser la stratégie de versioning
git flow init
# installer les dépendances
yarn install
```

## Utilisation
### Lignes de commande
```bash
# Lancer le serveur de développement
yarn dev
# Construire la version de production
yarn build
# Lancer les tests end-to-end
yarn test:e2e
# Lancer les tests unitaires
yarn test:unit
# Lancer le linter
yarn test:lint
```

## Deployement

Pour déployer le projet en production
```bash
# créer la release
git flow release start <v.X.X.X>

# publier la release sur le dépo si besoin
git flow release publish <v.X.X.X>

# terminer la release
git flow release finish <v.X.X.X>
git push --tags
```
