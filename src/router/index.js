import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import i18n from '@/i18n'
import routes from './routes'

Vue.use(Router)
Vue.use(Meta, {
  keyName: 'head',
  attribute: 'data-head',
  tagIDKeyName: 'id'
})

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes,
  linkActiveClass: 'is-active',
  linkExactActiveClass: 'is-exact-and-active'
})

// on route appear or change...
router.beforeEach((to, from, next) => {
  syncLang(to, from, next)
})

const syncLang = (to, from, next) => {
  // TODO : rendre ça plus propre
  let resetRouteLocal = false
  let resetLocal

  if ((to.query.hl && from.query.hl) &&
    (from.query.hl !== to.query.hl)) {
    if (to.name !== from.name) {
      // the lang must be reset
      resetRouteLocal = true
      resetLocal = from.query.hl
    }
  }
  if (from.query.hl && !to.query.hl) {
    // if the previous route contains the language query string
    // we place this one on the next route (route1/?hl=fr => route2/?hl=fr)
    next({
      path: to.path,
      query: {
        hl: i18n.locale
      }
    })
  } else {
    if (resetRouteLocal) {
      next({
        path: to.path,
        query: {
          hl: resetLocal
        }
      })
    } else {
      next()
    }
  }
}

export default router
